#![no_main]
#![no_std]

extern crate panic_halt;

use cortex_m::peripheral::syst::SystClkSource;
use cortex_m::Peripherals;
use cortex_m_rt::{entry, exception};
use cortex_m_semihosting::hprint;

#[entry]
fn main() -> ! {
    let systick = get_systick();
    let time = get_time();

    loop {}
}

