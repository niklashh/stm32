#![no_main]
#![no_std]

extern crate panic_halt;

use cortex_m_rt::{entry};

#[entry]
fn main() -> ! {
    // read a nonexistent memory location
    unsafe {
        const GPIOC_BSRR: u32 = 0x40011010;

        *(GPIOC_BSRR as *mut u32) = 1 << 13;
    }

    loop {}
}
